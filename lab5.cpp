/**********
Toni Brennan
ahbrenn
Lab 5
Lab Section: 005
Alex & Hollis
**********/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
//#include <cstdlib.h>
//#include <ctime>
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck[52];
   int i = 0;            //for loop
   int j = 2;           //between 2-Ace
   Suit code = SPADES;  //will help assign the deck with suits
   int c = 0;           //will help change code

   //creates the deck
   for(i = 0; i < 52; ++i){
      if(j > 14){
        j = 2;
        c++;
        if(c == 1){
          code = HEARTS;
        }
        else if(c == 2){
          code = DIAMONDS;
        }
        else if(c == 3){
          code = CLUBS;
        }
      }
      deck[i].suit = code;
      deck[i].value = j;
      j++;
   }

  //shuffles the deck
   random_shuffle(&deck[0], &deck[52]);

   //gives the 5 top cards from the shuffled deck
    Card hand[5];
    for(int d = 0; d < 5; d++){
      hand[d] = deck[d];
    }

    //sorts the cards
     sort(&hand[0], &hand[5], suit_order);

     //should print the cards
     for(int e = 0; e < 5; e++){
       if(hand[e].value < 11){
         std::cout.width(10); std::cout << std::right << hand[e].value;
       }
       if(hand[e].value > 10){
         std::cout.width(10); std::cout << std::right << get_card_name(hand[e]);
       }
       cout << " of " <<get_suit_code(hand[e]) << endl;
     }
  //program ends
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  bool order;
  //compares the suits
  if (lhs.suit < rhs.suit){
    order = true;
    return order;
  }
  //if suits match, will compare the numbers
  else if (lhs.suit == rhs.suit){
    if (lhs.value < rhs.value){
      order = true;
    }
    else{
      order = false;
    }
  }
  //if lhs > rhs
  else{
    order = false;
  }

  return order;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  switch (c.value) {
    case 11:    return "Jack";
    case 12:    return "Queen";
    case 13:    return "King";
    case 14:    return "Ace";
    default:    return "";
  }
}
